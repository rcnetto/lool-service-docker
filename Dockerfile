FROM rcnetto/lool-build-deps:cd-3.2.2-8-built-pre-install

LABEL maintainer="rcnetto@gmail.com"

WORKDIR /opt/online
RUN make install
RUN mkdir -p /usr/local/var/cache/loolwsd && \
    chown -R lool:lool /usr/local/var/cache/loolwsd && \
    mkdir -p /var/cache/loolwsd && \
    chown -R lool:lool /var/cache/loolwsd
RUN mkdir /etc/loolwsd
RUN mv /opt/online/loolwsd.xml /usr/local/etc/loolwsd/loolwsd.xml
RUN touch /var/log/loolwsd.log && \
    chown lool:lool /var/log/loolwsd.log
RUN mkdir /opt/online/jails && \
    chown lool:lool /opt/online/jails && \
    mkdir /opt/online/systemplate && \
    chown lool:lool /opt/online/systemplate
RUN su lool -c "loolwsd-systemplate-setup /opt/online/systemplate /opt/libreoffice-instdir/instdir >/dev/null 2>&1"
RUN openssl genrsa -out /etc/loolwsd/key.pem 4096 && \
  openssl req -out /etc/loolwsd/cert.csr -key /etc/loolwsd/key.pem -new -sha256 -nodes -subj "/C=DE/OU=onlineoffice-install.com/CN=onlineoffice-install.com/emailAddress=nomail@nodo.com" && \
  openssl x509 -req -days 1825 -in /etc/loolwsd/cert.csr -signkey /etc/loolwsd/key.pem -out /etc/loolwsd/cert.pem && \
  openssl x509 -req -days 1825 -in /etc/loolwsd/cert.csr -signkey /etc/loolwsd/key.pem -out /etc/loolwsd/ca-chain.cert.pem && \
  chown lool:lool /etc/loolwsd/key.pem && \
  chmod 600 /etc/loolwsd/key.pem
COPY /loolwsd.service /lib/systemd/system/
RUN touch /lib/systemd/system/loolwsd.service
RUN ln /lib/systemd/system/loolwsd.service /etc/systemd/system/$loolwsd_service_name.service


COPY /run-lool.sh /usr/local/bin/
RUN ["chmod", "+x", "/usr/local/bin/run-lool.sh"]
CMD bash /usr/local/bin/run-lool.sh

#docker run -t -d -p 9980:9980 -e "domain=localhost" --restart no --cap-add MKNOD lool-docker:latest
# envio para o docker
#docker build -t lool-docker .
#docker login --username=rcnetto
#docker tag xxxxxxxxxxxx rcnetto/lool-docker:3.2.2-8
#docker push rcnetto/lool-docker:3.2.2-8