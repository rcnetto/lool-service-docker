#!/bin/bash
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

export LC_CTYPE=en_US.UTF-8

# Fix domain name resolution from jails
cp /etc/resolv.conf /etc/hosts /opt/online/systemplate/etc/

# Replace trusted host
perl -pi -e "s/localhost<\/host>/${domain}<\/host>/g" /usr/local/etc/loolwsd/loolwsd.xml
perl -pi -e "s/<username (.*)>.*<\/username>/<username \1>${username}<\/username>/" /usr/local/etc/loolwsd/loolwsd.xml
perl -pi -e "s/<password (.*)>.*<\/password>/<password \1>${password}<\/password>/" /usr/local/etc/loolwsd/loolwsd.xml


# Restart when /etc/loolwsd/loolwsd.xml changes
[ -x /usr/bin/inotifywait -a /usr/bin/killall ] && (
	/usr/bin/inotifywait -e modify /etc/loolwsd/loolwsd.xml
	echo "$(ls -l /usr/local/etc/loolwsd/loolwsd.xml) modified --> restarting"
	/usr/bin/killall -1 loolwsd
) &

# Start loolwsd
su -c "/opt/online/loolwsd --o:sys_template_path=/opt/online/systemplate --o:lo_template_path=/opt/libreoffice-instdir/instdir  --o:child_root_path=/opt/online/jails ${extra_params}" -s /bin/bash lool